#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int MAX_N = 10000000;

int n;
int x[MAX_N];
int buff[MAX_N];

void read_input()
{
  scanf("%d",&n);
  for(int i=0; i<n; i++)
    scanf("%d",&x[i]);
}

void output_summary()
{
  int t = 0;
  int shift = 0;
  for(int i=0; i<n; i++) {
    t += ((x[i] + i) << shift);
    shift += 1;
    if(shift > 8)
      shift = 0;
  }
  printf("%d\n",t);
}

void output()
{
  for(int i=0; i<n; i++) 
    printf("%d\n",x[i]);
}

void merge(int s1, int s2, int t2)
{
  int t1 = s2;
  int bi = s1;
  int ss1 = s1;
  while((s1 < t1) && (s2 < t2)) {
    if(x[s1] < x[s2]) {
      buff[bi] = x[s1];
      s1++;
    } else {
      buff[bi] = x[s2];
      s2++;
    }
    bi++;
  }
  while(s1 < t1) {
    buff[bi] = x[s1];
    s1++;
    bi++;
  }
  while(s2 < t2) {
    buff[bi] = x[s2];
    s2++;
    bi++;
  }
  for(int i = ss1; i < t2; i++)
    x[i] = buff[i];
}

void mergesort(int x[], int s, int t)
{
  if(s >= t-1)
    return;
  int m = (s + t - 1)/2;
  mergesort(x, s, m+1);
  mergesort(x, m+1, t);
  merge(s, m+1, t);
}
 
int main()
{
  read_input();
  mergesort(x,0,n);
  output_summary();
}
