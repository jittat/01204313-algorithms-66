// Quicksort implementation from https://www.geeksforgeeks.org/quick-sort/

#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int MAX_N = 10000000;

int n;
int x[MAX_N];
int buff[MAX_N];

void read_input()
{
  scanf("%d",&n);
  for(int i=0; i<n; i++)
    scanf("%d",&x[i]);
}

void output_summary()
{
  int t = 0;
  int shift = 0;
  for(int i=0; i<n; i++) {
    t += ((x[i] + i) << shift);
    shift += 1;
    if(shift > 8)
      shift = 0;
  }
  printf("%d\n",t);
}

void output()
{
  for(int i=0; i<n; i++) 
    printf("%d\n",x[i]);
}

// from https://www.geeksforgeeks.org/quick-sort/
int partition(int arr[],int low,int high)
{
  //choose the pivot
   
  int pivot=arr[high];
  //Index of smaller element and Indicate
  //the right position of pivot found so far
  int i=(low-1);
   
  for(int j=low;j<=high;j++)
  {
    //If current element is smaller than the pivot
    if(arr[j]<pivot)
    {
      //Increment index of smaller element
      i++;
      swap(arr[i],arr[j]);
    }
  }
  swap(arr[i+1],arr[high]);
  return (i+1);
}
 
// The Quicksort function Implement
            
// from https://www.geeksforgeeks.org/quick-sort/
void quicksort(int arr[],int low,int high)
{
  // when low is less than high
  if(low<high)
  {
    // pi is the partition return index of pivot
     
    int pi=partition(arr,low,high);
     
    //Recursion Call
    //smaller element than pivot goes left and
    //higher element goes right
    quicksort(arr,low,pi-1);
    quicksort(arr,pi+1,high);
  }
}

int main()
{
  read_input();
  quicksort(x,0,n-1);
  output_summary();
}
