#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int MAX_N = 10000000;

int n;
int x[MAX_N];
int buff[MAX_N];

void read_input()
{
  scanf("%d",&n);
  for(int i=0; i<n; i++)
    scanf("%d",&x[i]);
}

void output_summary()
{
  int t = 0;
  int shift = 0;
  for(int i=0; i<n; i++) {
    t += ((x[i] + i) << shift);
    shift += 1;
    if(shift > 8)
      shift = 0;
  }
  printf("%d\n",t);
}

void output()
{
  for(int i=0; i<n; i++) 
    printf("%d\n",x[i]);
}

void insertion_sort(int x[], int n)
{
  for(int i=1; i<n; i++) {
    int j = i;
    while((j > 0) && (x[j] < x[j-1])) {
      int tmp = x[j];
      x[j] = x[j-1];
      x[j-1] = tmp;
      j--;
    }
  }
}
 
int main()
{
  read_input();
  insertion_sort(x,n);
  output_summary();
}
