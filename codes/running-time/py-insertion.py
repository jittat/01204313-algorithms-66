from random import randint
import sys

def insertion_sorted(x,n):
    for i in range(n-1):
        j = i + 1
        while (j > 0) and (x[j] < x[j-1]):
            x[j],x[j-1] = x[j-1],x[j]
            j -= 1
    return x

n = int(input())
x = [int(input()) for i in range(n)]
xs = insertion_sorted(x,n)


