#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

const int MAX_N = 10000000;

int n;
int x[MAX_N];

void read_input()
{
  scanf("%d",&n);
  for(int i=0; i<n; i++)
    scanf("%d",&x[i]);
}

void output()
{
  int t = 0;
  int shift = 0;
  for(int i=0; i<n; i++) {
    t += ((x[i] + i) << shift);
    shift += 1;
    if(shift > 8)
      shift = 0;
  }
  printf("%d\n",t);
}

void bubblesort(int x[], int n)
{
  while(true) {
    bool moved = false;
    for(int i=0; i<n-1; i++) {
      if(x[i+1] < x[i]) {
	int tmp = x[i+1];
	x[i+1] = x[i];
	x[i] = tmp;
	moved = true;
      }
    }
    if(!moved)
      break;
  }
}

int main()
{
  read_input();
  bubblesort(x,n);
  output();
}
