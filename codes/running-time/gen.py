from random import randint
import sys

n = int(sys.argv[1])
mx = 50000000

xs = [randint(1,mx) for i in range(n)]
if (len(sys.argv) > 2) and sys.argv[2] == '--sorted':
    xs = sorted(xs)

print(n)
for i in range(n):
    print(xs[i])

