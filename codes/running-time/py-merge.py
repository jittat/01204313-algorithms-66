from random import randint
import sys

def merge(x,y):
    nx = len(x)
    ny = len(y)
    ix = 0
    iy = 0
    res = []
    while (ix < nx) and (iy < ny):
        if x[ix] < y[iy]:
            res.append(x[ix])
            ix += 1
        else:
            res.append(y[iy])
            iy += 1
    while ix < nx:
        res.append(x[ix])
        ix += 1
    while iy < ny:
        res.append(y[iy])
        iy += 1
    return res

def mergesort(xs):
    n = len(xs)
    if n <= 1:
        return xs

    m = n // 2
    left = mergesort(xs[0:m])
    right = mergesort(xs[m:])
    return merge(left,right)

n = int(input())
x = [int(input()) for i in range(n)]
xs = mergesort(x)


